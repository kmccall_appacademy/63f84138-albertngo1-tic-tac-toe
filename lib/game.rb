require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_accessor :player_one, :player_two, :current_player, :board

  def initialize(player_one, player_two)
    @player_one, @player_two = player_one, player_two
    @board = Board.new
    @current_player = player_one
    player_one.mark = :X
    player_two.mark = :O

  end

  def play_turn
    board.place_mark(current_player.get_move, current_player.mark)

    switch_players!

    current_player.display(board)

  end

  def switch_players!

    if current_player == player_one
      @current_player = player_two
    elsif current_player == player_two
      @current_player = player_one
    end

  end

  def play

    current_player.display(board)

    until board.over?
      play_turn
    end

    if game_winner
      game_winner.display(board)
      puts "The winner is #{game_winner.name}. Congrats"
    else
      puts "Tie"
    end

  end

  def game_winner
    return player_one if board.winner == player_one.mark
    return player_two if board.winner == player_two.mark
  end
end


if $PROGRAM_NAME == __FILE__

  print "Enter your name \n"
  name = gets.chomp.strip

  human = HumanPlayer.new(name)

  garry = ComputerPlayer.new("Garry")

  new_game = Game.new(human, garry)
  new_game.play
end
