class ComputerPlayer
  attr_accessor :mark
  attr_reader :name, :board

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    moves = []
    (0..2).each do |row|
      (0..2).each do |col|
        pos = [row, col]
        moves << pos if board[pos].nil?
      end
    end

    moves.each do |move|
      return move if wins?(move)
    end
    moves.sample
  end

  def wins?(pos)
    board[pos] = mark
    if board.winner == mark
      board[pos] = nil
      true
    else
      board[pos] = nil
      false
    end
  end
end
