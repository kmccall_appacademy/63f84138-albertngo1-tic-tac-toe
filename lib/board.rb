class Board
  attr_reader :grid, :marks

  def self.grid
    Array.new(3) {Array.new(3)}
  end

  def initialize(grid = Board.grid)
    @grid = grid
    @marks = [:X, :O]
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, val)
    row, col = pos
    @grid[row][col] = val
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    (grid + cols + diag).each do |triple|
      return :X if triple == [:X, :X, :X]
      return :O if triple == [:O, :O, :O]
    end
    nil
  end

  def cols
    cols = [[], [], []]
    grid.each do |row|
      row.each_with_index do |mark, idx|
        cols[idx] << mark
      end
    end
    cols
  end

  def diag
    down_diag = [[0, 0], [1, 1], [2, 2]]
    up_diag = [[2, 0], [1, 1], [2, 0]]

    [down_diag, up_diag].map do |diagonal|
      diagonal.map { |row, col| grid[row][col] }
    end
  end

  def over?
    grid.flatten.none?(&:nil?) || winner
  end

end
